#ifndef __TIM_H__
#define __TIM_H__

#include "main.h"

void TIM2_Init(void);
void TIM3_Init(void);

uint16_t map(uint16_t val, uint16_t inMin, uint16_t inMax, uint16_t outMin, uint16_t outMax);

#endif /* ifndef __TIM_H__ */
